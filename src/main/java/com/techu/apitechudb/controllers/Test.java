package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;

public class Test {

    private String name;
    private float price;
    private ProductModel productModel;

    public Test() {

    }

    public Test(String name, float price, ProductModel productModel) {
        this.name = name;
        this.price = price;
        this.productModel = productModel;
    }

}
