package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    //Listar todos los usuarios con una determinada edad
    public List<UserModel> findAll(){
        System.out.println("findAll en UserRepository");

        //Stream() -> enlaza varias funciones en lambda
        //return ApitechudbApplication.userModels.stream().filter(userModel -> userModel.getAge() == ageFilter).collect(Collectors.toList());
        return ApitechudbApplication.userModels;
    }

    //Encontrar usuario por ID
    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserRepository");

        Optional<UserModel> result = Optional.empty();

        for (UserModel userInList : ApitechudbApplication.userModels) {
            if(userInList.getId().equals(id)) {
                System.out.println("Usuario encontrado");
                result = Optional.of(userInList);
            }
        }
        return result;
    }

    //Añadir usuario
    public UserModel save(UserModel user){
        System.out.println("Save en UserRepository");

        ApitechudbApplication.userModels.add(user);

        return user;
    }


    //Actualizar usuario
    public UserModel update(UserModel user) {
        System.out.println("update en UserRepository");

        Optional<UserModel> userToUpdate =  this.findById(user.getId());

        if (userToUpdate.isPresent() == true){
            System.out.println("Usuario para actualizar encontrado");

            UserModel userFromList = userToUpdate.get();

            userFromList.setName(user.getName());
            userFromList.setAge(user.getAge());
        }
        return user;
    }

    //Borrar usuario
    public void delete (UserModel user){

        System.out.println("delete en UserRepository");
        System.out.println("borrando usuario");

        ApitechudbApplication.productModels.remove(user);
    }
}
